use std::ops::Range;

use rand::{rngs::ThreadRng, seq::IteratorRandom};

#[derive(Debug)]
pub struct Maze {
    columns: usize,
    rows: usize,
    removed_walls: Vec<WallId>,
}

impl Maze {
    pub fn random(columns: usize, rows: usize) -> Self {
        MazeGenerator::generate(columns, rows)
    }

    pub fn columns(&self) -> usize {
        self.columns
    }

    pub fn rows(&self) -> usize {
        self.rows
    }

    pub fn cells(&self) -> Cells<'_> {
        Cells::new(self)
    }

    fn neighbours(&self, cell_id: CellId) -> Vec<CellId> {
        [
            self.north_neighbour_of(cell_id),
            self.west_neighbour_of(cell_id),
            self.south_neighbour_of(cell_id),
            self.east_neighbour_of(cell_id),
        ]
        .into_iter()
        .flatten()
        .collect::<Vec<_>>()
    }

    fn north_neighbour_of(&self, cell_id: CellId) -> Option<CellId> {
        if self.has_north_neighbour(cell_id) {
            Some(cell_id - self.columns)
        } else {
            None
        }
    }

    fn west_neighbour_of(&self, cell_id: CellId) -> Option<CellId> {
        if self.has_west_neighbour(cell_id) {
            Some(cell_id + 1)
        } else {
            None
        }
    }

    fn south_neighbour_of(&self, cell_id: CellId) -> Option<CellId> {
        if self.has_south_neighbour(cell_id) {
            Some(cell_id + self.columns)
        } else {
            None
        }
    }

    fn east_neighbour_of(&self, cell_id: CellId) -> Option<CellId> {
        if self.has_east_neighbour(cell_id) {
            Some(cell_id - 1)
        } else {
            None
        }
    }

    fn has_north_neighbour(&self, cell_id: CellId) -> bool {
        cell_id >= self.columns
    }

    fn has_west_neighbour(&self, cell_id: CellId) -> bool {
        cell_id % self.columns < self.columns - 1
    }

    fn has_south_neighbour(&self, cell_id: CellId) -> bool {
        cell_id / self.columns < self.rows - 1
    }

    fn has_east_neighbour(&self, cell_id: CellId) -> bool {
        cell_id % self.columns > 0
    }

    fn column_of(&self, cell_id: CellId) -> usize {
        cell_id % self.columns
    }

    fn row_of(&self, cell_id: CellId) -> usize {
        cell_id / self.columns
    }
}

/// Pair of two cell indexes next to a wall, uniquely identifing that wall.
#[derive(Debug, PartialEq, Eq, Clone, Copy)]
struct WallId(CellId, CellId);

impl WallId {
    /// Creates a new wall, if `a` and `b` are neighbours.
    pub fn new(maze: &Maze, a: CellId, b: CellId) -> Option<Self> {
        if !maze.neighbours(a).contains(&b) {
            None
        } else {
            Some(Self(a.min(b), a.max(b)))
        }
    }
}

type CellId = usize;

#[derive(Debug, PartialEq)]
pub struct Cell {
    column: usize,
    row: usize,
    north_open: bool,
    west_open: bool,
    south_open: bool,
    east_open: bool,
}

impl Cell {
    pub fn id(&self) -> CellId {
        self.row + self.column
    }

    pub fn is_north_open(&self) -> bool {
        self.north_open
    }

    pub fn is_west_open(&self) -> bool {
        self.west_open
    }

    pub fn is_south_open(&self) -> bool {
        self.south_open
    }

    pub fn is_east_open(&self) -> bool {
        self.east_open
    }

    pub fn entrances(&self) -> usize {
        [
            self.is_north_open(),
            self.is_west_open(),
            self.is_south_open(),
            self.is_east_open(),
        ]
        .into_iter()
        .map(|open| if open { 1 } else { 0 })
        .sum()
    }

    pub fn column(&self) -> usize {
        self.column
    }

    pub fn row(&self) -> usize {
        self.row
    }
}

/// Iterator over maze cells.
pub struct Cells<'a> {
    maze: &'a Maze,
    range: Range<CellId>,
}

impl<'a> Cells<'a> {
    fn new(maze: &'a Maze) -> Self {
        Self {
            maze,
            range: 0..maze.columns * maze.rows,
        }
    }

    fn is_neighbour_connection_open(&self, cell_id: CellId, neighbour: Option<CellId>) -> bool {
        neighbour
            .and_then(|neighbour_id| WallId::new(&self.maze, cell_id, neighbour_id))
            .map(|wall| self.maze.removed_walls.contains(&wall))
            .unwrap_or_default()
    }
}

impl<'a> Iterator for Cells<'a> {
    type Item = Cell;

    fn next(&mut self) -> Option<Self::Item> {
        let cell_index = self.range.next()?;

        Some(Self::Item {
            column: self.maze.column_of(cell_index),
            row: self.maze.row_of(cell_index),
            north_open: self
                .is_neighbour_connection_open(cell_index, self.maze.north_neighbour_of(cell_index)),
            west_open: self
                .is_neighbour_connection_open(cell_index, self.maze.west_neighbour_of(cell_index)),
            south_open: self
                .is_neighbour_connection_open(cell_index, self.maze.south_neighbour_of(cell_index)),
            east_open: self
                .is_neighbour_connection_open(cell_index, self.maze.east_neighbour_of(cell_index)),
        })
    }
}

/// Generates mazes using randomized depth first search.
struct MazeGenerator {
    /// A maze beign generated.
    maze: Maze,

    /// Random number generator used to get a random neighbour.
    rng: ThreadRng,

    /// Already visited cell ids.
    visited_cells_ids: Vec<CellId>,

    /// Ids of cells that are currently beign visited or are waiting for
    /// that to happen.
    queued_cells_ids: Vec<CellId>,
}

impl MazeGenerator {
    const INITIAL_CELL_ID: usize = 0;

    fn generate(columns: usize, rows: usize) -> Maze {
        let mut generator = Self::new(columns, rows);

        while let Some(cell_index) = generator.next_cell() {
            generator.visit(cell_index);
        }

        generator.maze
    }

    fn new(columns: usize, rows: usize) -> MazeGenerator {
        Self {
            maze: Maze {
                rows,
                columns,
                removed_walls: Vec::new(),
            },
            rng: rand::thread_rng(),
            visited_cells_ids: Vec::new(),
            queued_cells_ids: vec![Self::INITIAL_CELL_ID],
        }
    }

    fn next_cell(&mut self) -> Option<CellId> {
        self.queued_cells_ids.pop()
    }

    fn visit(&mut self, cell_id: CellId) {
        let choosen_cell_id = self.choose_neighbour(cell_id);

        if let Some(choosen_cell_id) = choosen_cell_id {
            self.queued_cells_ids.push(cell_id);
            self.maze
                .removed_walls
                .push(WallId::new(&self.maze, choosen_cell_id, cell_id).unwrap());
            self.visited_cells_ids.push(choosen_cell_id);
            self.queued_cells_ids.push(choosen_cell_id);
        }
    }

    fn choose_neighbour(&mut self, cell_id: CellId) -> Option<usize> {
        self.maze
            .neighbours(cell_id)
            .into_iter()
            .filter(|index| !self.visited_cells_ids.contains(index))
            .choose(&mut self.rng)
    }
}
