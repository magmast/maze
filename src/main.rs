use bevy::prelude::*;
use maze::game::GamePlugin;

fn main() {
    App::new().add_plugin(GamePlugin).run()
}
