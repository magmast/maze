use bevy::prelude::*;
use bevy_iced::{
    iced::{self, widget, Alignment, Length},
    IcedContext,
};

use crate::game::GameState;

pub struct WinPlugin;

impl Plugin for WinPlugin {
    fn build(&self, app: &mut App) {
        app.add_event::<WinEvent>()
            .add_system_set(SystemSet::on_enter(GameState::Loading).with_system(load_win_assets))
            .add_system_set(SystemSet::on_enter(GameState::Winned).with_system(play_sound))
            .add_system_set(
                SystemSet::on_update(GameState::Winned)
                    .with_system(show_win)
                    .with_system(handle_win_events)
                    .with_system(handle_space_or_enter),
            );
    }
}

#[derive(Resource)]
struct WinAssets {
    sound: Handle<AudioSource>,
}

#[derive(Clone)]
enum WinEvent {
    RestartPressed,
    MainMenuPressed,
}

fn load_win_assets(mut commands: Commands, asset_server: Res<AssetServer>) {
    commands.insert_resource(WinAssets {
        sound: asset_server.load("win.ogg"),
    });
}

fn show_win(mut ctx: IcedContext<WinEvent>) {
    ctx.display(
        crate::fill_container(
            iced::column!(
                widget::text("YOU WON!"),
                widget::vertical_space(Length::Units(32)),
                widget::button("Restart").on_press(WinEvent::RestartPressed),
                widget::vertical_space(Length::Units(16)),
                widget::button("Main Menu").on_press(WinEvent::MainMenuPressed),
            )
            .align_items(Alignment::Center),
        )
        .center_x()
        .center_y(),
    );
}

fn handle_win_events(mut app_state: ResMut<State<GameState>>, mut messages: EventReader<WinEvent>) {
    for message in messages.iter() {
        match message {
            WinEvent::RestartPressed => app_state.set(GameState::Playing).unwrap(),
            WinEvent::MainMenuPressed => app_state.set(GameState::MainMenu).unwrap(),
        }
    }
}

fn handle_space_or_enter(mut app_state: ResMut<State<GameState>>, input: Res<Input<KeyCode>>) {
    // TODO(magmast): Handle enter.
    if input.just_pressed(KeyCode::Space) {
        app_state.set(GameState::Playing).unwrap();
    }
}

fn play_sound(assets: Res<WinAssets>, audio: Res<Audio>) {
    audio.play(Handle::clone(&assets.sound));
}
