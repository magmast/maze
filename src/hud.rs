use bevy::prelude::*;
use bevy_iced::{
    iced::{
        self,
        alignment::{Horizontal, Vertical},
        Alignment, Padding,
    },
    iced::{widget, Length},
    IcedContext,
};

use crate::game::GameState;

pub struct HudPlugin;

impl Plugin for HudPlugin {
    fn build(&self, app: &mut App) {
        app.add_event::<HudEvent>().add_system_set(
            SystemSet::on_update(GameState::Playing)
                .with_system(show_hud)
                .with_system(handle_hud_messages),
        );
    }
}

#[derive(Clone)]
enum HudEvent {
    RestartPressed,
    MainMenuPressed,
}

fn show_hud(mut ctx: IcedContext<HudEvent>) {
    ctx.display(
        crate::fill_container(
            iced::column!(
                widget::button("Restart").on_press(HudEvent::RestartPressed),
                widget::vertical_space(Length::Units(8)),
                widget::button("Main Menu").on_press(HudEvent::MainMenuPressed),
            )
            .align_items(Alignment::End),
        )
        .width(Length::Fill)
        .height(Length::Fill)
        .align_x(Horizontal::Right)
        .align_y(Vertical::Bottom)
        .padding(Padding::new(16)),
    );
}

fn handle_hud_messages(mut state: ResMut<State<GameState>>, mut events: EventReader<HudEvent>) {
    for event in events.iter() {
        match event {
            HudEvent::MainMenuPressed => state.set(GameState::MainMenu).unwrap(),
            HudEvent::RestartPressed => state.set(GameState::Restarting).unwrap(),
        }
    }
}
