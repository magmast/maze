use bevy::prelude::*;

/// Positions and scales grid cells.
///
/// Every entity with the `Cell` component must be a direct child of an entity
/// with the `Grid` component. If a `Grid` isn't a direct parent of a `Cell`,
/// then it's ignored.
///
/// Currently all `Grid`s have size of a window or (if squred is `true`) size of
/// a smaller dimension.
pub struct GridPlugin;

impl Plugin for GridPlugin {
    fn build(&self, app: &mut App) {
        app.add_system(position_and_scale_cells);
    }
}

fn position_and_scale_cells(
    windows: Res<Windows>,
    grids_query: Query<(&Grid, &Children)>,
    mut cells_query: Query<(&Cell, &mut Transform)>,
) {
    let window = windows.primary();

    for (grid, children) in &grids_query {
        for child in children {
            if let Ok((cell, mut transform)) = cells_query.get_mut(*child) {
                let cell_size_px = grid.cell_size_px(Vec2::new(window.width(), window.height()));
                let child_scale_x = cell_size_px.x / cell.size.x * cell.scale.x;
                let child_scale_y = cell_size_px.y / cell.size.y * cell.scale.y;
                let child_position_px = grid.cell_position_px(cell.position, cell_size_px);
                transform.translation = Vec3::new(
                    child_position_px.x,
                    child_position_px.y,
                    transform.translation.z,
                );
                transform.scale = Vec3::new(child_scale_x, child_scale_y, transform.scale.y);
            }
        }
    }
}

#[derive(Component)]
pub struct Grid {
    pub columns: usize,
    pub rows: usize,
    pub square: bool,
}

impl Grid {
    fn cell_position_px(&self, cell_position: Vec2, cell_size_px: Vec2) -> Vec2 {
        let offset_x_px = -(self.columns as f32 - 1.0) * cell_size_px.x / 2.0;
        let offset_y_px = (self.rows as f32 - 1.0) * cell_size_px.y / 2.0;
        let target_x_px = offset_x_px + cell_position.x * cell_size_px.x;
        let target_y_px = offset_y_px + cell_position.y * -cell_size_px.y;
        Vec2::new(target_x_px, target_y_px)
    }

    fn cell_size_px(&self, parent_size: Vec2) -> Vec2 {
        let x = parent_size.x / self.columns as f32;
        let y = parent_size.y / self.rows as f32;
        if self.square {
            let scale = x.min(y);
            Vec2::new(scale, scale)
        } else {
            Vec2::new(x, y)
        }
    }
}

#[derive(Component, Debug)]
pub struct Cell {
    pub size: Vec2,
    pub scale: Vec2,
    pub position: Vec2,
}

impl Default for Cell {
    fn default() -> Self {
        Self {
            size: Default::default(),
            scale: Vec2::new(1.0, 1.0),
            position: Default::default(),
        }
    }
}
