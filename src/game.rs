use bevy::prelude::*;
use bevy_iced::IcedPlugin;
use mazero::Maze as Mazero;
use rand::seq::IteratorRandom;

use crate::{
    grid::{Cell, GridPlugin},
    hud::HudPlugin,
    main_menu::MainMenuPlugin,
    maze::{spawn_maze, MazeAssets, MazePlugin},
    mouse::{spawn as spawn_mouse, Mouse, MouseAssets, MousePlugin},
    player::{spawn as spawn_player, Player, PlayerAssets, PlayerPlugin},
    win::WinPlugin,
};

pub struct GamePlugin;

impl Plugin for GamePlugin {
    fn build(&self, app: &mut App) {
        app.add_state(GameState::Loading)
            .insert_resource(ClearColor(Color::BLACK))
            .insert_resource(GameplayConfig::default())
            .add_plugins(DefaultPlugins.set(ImagePlugin::default_nearest()))
            .add_plugin(IcedPlugin)
            .add_plugin(GridPlugin)
            .add_plugin(MazePlugin)
            .add_plugin(PlayerPlugin)
            .add_plugin(MousePlugin)
            .add_plugin(WinPlugin)
            .add_plugin(HudPlugin)
            .add_plugin(MainMenuPlugin)
            .add_startup_system(spawn_camera)
            .add_system_set(
                SystemSet::on_update(GameState::Loading).with_system(exit_loading_state),
            )
            .add_system_set(SystemSet::on_enter(GameState::Playing).with_system(setup_playing))
            .add_system_set(SystemSet::on_update(GameState::Playing).with_system(look_for_win))
            .add_system_set(
                SystemSet::on_update(GameState::Restarting).with_system(quit_restarting_state),
            );
    }
}

#[derive(Hash, Clone, Copy, PartialEq, Eq, Debug)]
pub enum GameState {
    Loading,
    MainMenu,
    Playing,
    Restarting,
    Winned,
}

#[derive(Resource)]
pub struct GameplayConfig {
    pub maze_size: usize,
}

impl Default for GameplayConfig {
    fn default() -> Self {
        Self { maze_size: 10 }
    }
}

fn spawn_camera(mut commands: Commands) {
    commands.spawn(Camera2dBundle::default());
}

fn exit_loading_state(mut state: ResMut<State<GameState>>) {
    state.set(GameState::MainMenu).unwrap();
}

fn setup_playing(
    mut commands: Commands,
    config: Res<GameplayConfig>,
    maze_assets: Res<MazeAssets>,
    mouse_assets: Res<MouseAssets>,
    player_assets: Res<PlayerAssets>,
) {
    let maze = Mazero::random(config.maze_size, config.maze_size);
    let (player_position, mouse_position) = calculate_player_and_mouse_positions(&maze);

    let player = spawn_player(&mut commands, &player_assets, player_position);
    let mouse = spawn_mouse(&mut commands, &mouse_assets, mouse_position);

    spawn_maze(&mut commands, &maze_assets, maze)
        .add_child(player)
        .add_child(mouse);
}

fn quit_restarting_state(mut state: ResMut<State<GameState>>) {
    state.set(GameState::Playing).unwrap();
}

fn calculate_player_and_mouse_positions(maze: &Mazero) -> (Vec2, Vec2) {
    let mut rng = rand::thread_rng();

    let cells = maze
        .cells()
        .filter(|cell| cell.entrances() == 1)
        .choose_multiple(&mut rng, 2);

    let player_cell = &cells[0];
    let mouse_cell = &cells[1];

    (
        Vec2::new(player_cell.column() as f32, player_cell.row() as f32),
        Vec2::new(mouse_cell.column() as f32, mouse_cell.row() as f32),
    )
}

fn look_for_win(
    mut app_state: ResMut<State<GameState>>,
    mouse_query: Query<(With<Mouse>, &Cell)>,
    player_query: Query<(With<Player>, &Cell)>,
) {
    let (_, mouse_cell) = mouse_query.single();
    let (_, player_cell) = player_query.single();

    if mouse_cell.position.distance(player_cell.position) < 0.001 {
        app_state.set(GameState::Winned).unwrap();
    }
}
