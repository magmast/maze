use bevy::prelude::*;

use crate::{game::GameState, grid::Cell, maze::Maze};

const SPRITE_SHEET_PATH: &str = "cat/orange_002.png";
const SPRITE_SHEET_TILE_SIZE_PX: Vec2 = Vec2::new(32.0, 32.0);
const SPRITE_SHEET_COLUMNS: usize = 4;
const SPRITE_SHEET_ROWS: usize = 21;
const SPRITE_SHEET_PADDING: Option<Vec2> = None;
const SPRITE_SHEET_OFFSET: Option<Vec2> = None;
const SPEED: f32 = 10.0;
const SCALE: Vec2 = Vec2::new(0.7, 0.7);
const IDLE_FRAME_INDEX: usize = 0;
const NORTH_FRAME_INDEX: usize = 44;
const WEST_FRAME_INDEX: usize = 32;
const SOUTH_FRAME_INDEX: usize = 28;
const EAST_FRAME_INDEX: usize = 24;
/// Player position on z axis.
const Z: f32 = 20.0;
const MOVE_SOUND_PATH: &str = "move.ogg";
const INVALID_MOVE_SOUND_PATH: &str = "invalid_move.ogg";

/// `PlayerPlugin` does not spawn the player! Use `spawn` method in the
/// appriopriate system to spawn it. It works like that so you can create player
/// as the maze child.
pub struct PlayerPlugin;

impl Plugin for PlayerPlugin {
    fn build(&self, app: &mut App) {
        app.add_system_set(SystemSet::on_enter(GameState::Loading).with_system(load))
            .add_system_set(
                SystemSet::on_update(GameState::Playing)
                    .with_system(movement)
                    .with_system(update_position),
            );
    }
}

#[derive(Resource)]
pub struct PlayerAssets {
    texture_atlas: Handle<TextureAtlas>,
    move_sound: Handle<AudioSource>,
    invalid_move_sound: Handle<AudioSource>,
}

#[derive(Bundle, Default)]
pub struct PlayerBundle {
    pub sprite_sheet_bundle: SpriteSheetBundle,
    pub cell: Cell,
    pub player: Player,
}

#[derive(Component, Default)]
pub struct Player {
    pub target_position: Vec2,
}

fn load(
    mut commands: Commands,
    asset_server: Res<AssetServer>,
    mut texture_atlases: ResMut<Assets<TextureAtlas>>,
) {
    let texture_atlas = {
        let texture_handle = asset_server.load(SPRITE_SHEET_PATH);
        let texture_atlas = TextureAtlas::from_grid(
            texture_handle,
            SPRITE_SHEET_TILE_SIZE_PX,
            SPRITE_SHEET_COLUMNS,
            SPRITE_SHEET_ROWS,
            SPRITE_SHEET_PADDING,
            SPRITE_SHEET_OFFSET,
        );
        texture_atlases.add(texture_atlas)
    };

    let move_sound = asset_server.load(MOVE_SOUND_PATH);

    let invalid_move_sound = asset_server.load(INVALID_MOVE_SOUND_PATH);

    commands.insert_resource(PlayerAssets {
        texture_atlas,
        move_sound,
        invalid_move_sound,
    });
}

pub fn spawn(commands: &mut Commands, assets: &PlayerAssets, initial_position: Vec2) -> Entity {
    commands
        .spawn(PlayerBundle {
            sprite_sheet_bundle: SpriteSheetBundle {
                texture_atlas: Handle::clone(&assets.texture_atlas),
                sprite: TextureAtlasSprite::new(IDLE_FRAME_INDEX),
                transform: Transform::from_xyz(0.0, 0.0, Z),
                ..default()
            },
            cell: Cell {
                scale: SCALE,
                size: SPRITE_SHEET_TILE_SIZE_PX,
                position: initial_position,
            },
            player: Player {
                target_position: initial_position,
            },
        })
        .id()
}

fn movement(
    input: Res<Input<KeyCode>>,
    audio: Res<Audio>,
    assets: Res<PlayerAssets>,
    maze_query: Query<&Maze>,
    mut player_query: Query<(&mut Player, &mut TextureAtlasSprite)>,
) {
    let maze = maze_query.single();

    for (mut player, mut sprite) in &mut player_query {
        let cell = maze.cells().find(|cell| {
            cell.column() == player.target_position.x as usize
                && cell.row() == player.target_position.y as usize
        });

        if let Some(cell) = cell {
            if input.just_pressed(KeyCode::Up) {
                if cell.is_north_open() {
                    player.target_position.y -= 1.0;
                    *sprite = TextureAtlasSprite::new(NORTH_FRAME_INDEX);
                    audio.play(Handle::clone(&assets.move_sound));
                } else {
                    audio.play(Handle::clone(&assets.invalid_move_sound));
                }
            }
            if input.just_pressed(KeyCode::Right) {
                if cell.is_west_open() {
                    player.target_position.x += 1.0;
                    *sprite = TextureAtlasSprite::new(WEST_FRAME_INDEX);
                    audio.play(Handle::clone(&assets.move_sound));
                } else {
                    audio.play(Handle::clone(&assets.invalid_move_sound));
                }
            }
            if input.just_pressed(KeyCode::Down) {
                if cell.is_south_open() {
                    player.target_position.y += 1.0;
                    *sprite = TextureAtlasSprite::new(SOUTH_FRAME_INDEX);
                    audio.play(Handle::clone(&assets.move_sound));
                } else {
                    audio.play(Handle::clone(&assets.invalid_move_sound));
                }
            }
            if input.just_pressed(KeyCode::Left) {
                if cell.is_east_open() {
                    player.target_position.x -= 1.0;
                    *sprite = TextureAtlasSprite::new(EAST_FRAME_INDEX);
                    audio.play(Handle::clone(&assets.move_sound));
                } else {
                    audio.play(Handle::clone(&assets.invalid_move_sound));
                }
            }
        }
    }
}

fn update_position(time: Res<Time>, mut query: Query<(&Player, &mut Cell)>) {
    for (player, mut cell) in &mut query {
        cell.position = cell
            .position
            .lerp(player.target_position, time.delta().as_secs_f32() * SPEED);
    }
}
