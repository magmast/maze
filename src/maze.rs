use std::ops::Deref;

use bevy::{ecs::system::EntityCommands, prelude::*};
use mazero::Maze as Mazero;

use crate::{
    game::GameState,
    grid::{Cell, Grid},
};

const CELL_SPRITE_SHEET_PATH: &str = "cell.png";
const CELL_SPRITE_SHEET_TILE_SIZE_PX: Vec2 = Vec2::new(32.0, 32.0);
const CELL_SPRITE_SHEET_COLUMNS: usize = 4;
const CELL_SPRITE_SHEET_ROWS: usize = 4;
const CELL_SPRITE_SHEET_PADDING: Option<Vec2> = None;
const CELL_SPRITE_SHEET_OFFSET: Option<Vec2> = None;
const CELL_SCALE: Vec2 = Vec2::new(1.0, 1.0);

/// `MazePlugin` does not spawn the maze! Use the `spawn` function to spawn it.
/// It works like that so you can add children to the maze (like mouse or
/// player).
pub struct MazePlugin;

impl Plugin for MazePlugin {
    fn build(&self, app: &mut App) {
        app.add_system_set(SystemSet::on_enter(GameState::Loading).with_system(load_maze_assets))
            .add_system_set(
                SystemSet::on_enter(GameState::Restarting).with_system(crate::despawn::<Maze>),
            )
            .add_system_set(
                SystemSet::on_enter(GameState::MainMenu).with_system(crate::despawn::<Maze>),
            )
            .add_system_set(
                SystemSet::on_exit(GameState::Winned).with_system(crate::despawn::<Maze>),
            );
    }
}

#[derive(Resource)]
pub struct MazeAssets {
    texture_atlas: Handle<TextureAtlas>,
}

#[derive(Component, Debug)]
pub struct Maze(pub Mazero);

impl Deref for Maze {
    type Target = Mazero;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

fn load_maze_assets(
    mut commands: Commands,
    asset_server: Res<AssetServer>,
    mut texture_atlases: ResMut<Assets<TextureAtlas>>,
) {
    let texture_atlas = {
        let texture_handle = asset_server.load(CELL_SPRITE_SHEET_PATH);
        let texture_atlas = TextureAtlas::from_grid(
            texture_handle,
            CELL_SPRITE_SHEET_TILE_SIZE_PX,
            CELL_SPRITE_SHEET_COLUMNS,
            CELL_SPRITE_SHEET_ROWS,
            CELL_SPRITE_SHEET_PADDING,
            CELL_SPRITE_SHEET_OFFSET,
        );
        texture_atlases.add(texture_atlas)
    };

    commands.insert_resource(MazeAssets { texture_atlas });
}

pub fn spawn_maze<'a, 'w, 's>(
    commands: &'a mut Commands<'w, 's>,
    assets: &MazeAssets,
    maze: Mazero,
) -> EntityCommands<'w, 's, 'a> {
    let mut commands = commands.spawn((
        SpatialBundle::default(),
        Grid {
            columns: maze.columns(),
            rows: maze.rows(),
            square: true,
        },
    ));

    for cell in maze.cells() {
        let mut sprite_index = 0;
        if cell.is_north_open() {
            sprite_index |= 0b1000;
        }
        if cell.is_west_open() {
            sprite_index |= 0b0100;
        }
        if cell.is_south_open() {
            sprite_index |= 0b0010;
        }
        if cell.is_east_open() {
            sprite_index |= 0b0001;
        }

        commands.with_children(move |parent| {
            parent.spawn((
                SpriteSheetBundle {
                    texture_atlas: Handle::clone(&assets.texture_atlas),
                    sprite: TextureAtlasSprite::new(sprite_index),
                    ..default()
                },
                Cell {
                    position: Vec2::new(cell.column() as f32, cell.row() as f32),
                    scale: CELL_SCALE,
                    size: CELL_SPRITE_SHEET_TILE_SIZE_PX,
                },
            ));
        });
    }

    commands.insert(Maze(maze));

    commands
}
