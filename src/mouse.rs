use bevy::prelude::*;

use crate::{game::GameState, grid::Cell};

const MOUSE_TEXTURE_PATH: &str = "mouse.png";
const MOUSE_SCALE: Vec2 = Vec2::new(0.5, 0.5);
const MOUSE_SIZE_PX: Vec2 = Vec2::new(32.0, 32.0);
const MOUSE_Z: f32 = 10.0;

pub struct MousePlugin;

impl Plugin for MousePlugin {
    fn build(&self, app: &mut App) {
        app.add_system_set(SystemSet::on_enter(GameState::Loading).with_system(load));
    }
}

#[derive(Resource)]
pub struct MouseAssets {
    texture: Handle<Image>,
}

#[derive(Component)]
pub struct Mouse;

fn load(mut commands: Commands, asset_server: Res<AssetServer>) {
    commands.insert_resource(MouseAssets {
        texture: asset_server.load(MOUSE_TEXTURE_PATH),
    });
}

pub fn spawn(commands: &mut Commands, assets: &MouseAssets, initial_position: Vec2) -> Entity {
    commands
        .spawn((
            SpriteBundle {
                texture: Handle::clone(&assets.texture),
                transform: Transform::from_xyz(0.0, 0.0, MOUSE_Z),
                ..default()
            },
            Cell {
                position: initial_position,
                scale: MOUSE_SCALE,
                size: MOUSE_SIZE_PX,
            },
            Mouse,
        ))
        .id()
}
