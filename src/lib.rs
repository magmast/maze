pub mod game;
pub mod grid;
pub mod hud;
pub mod main_menu;
pub mod maze;
pub mod mouse;
pub mod player;
pub mod win;

use bevy::prelude::*;
use bevy_iced::iced::{
    widget::{container::StyleSheet, Container},
    Element, Length, Renderer,
};

pub fn despawn<C: Component>(mut commands: Commands, query: Query<Entity, With<C>>) {
    for entity in &query {
        commands.entity(entity).despawn_recursive();
    }
}

pub fn fill_container<'a, M, R>(content: impl Into<Element<'a, M, R>>) -> Container<'a, M, R>
where
    R: Renderer,
    R::Theme: StyleSheet,
{
    Container::new(content)
        .width(Length::Fill)
        .height(Length::Fill)
}
