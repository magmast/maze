use bevy::{app::AppExit, prelude::*};
use bevy_iced::{
    iced::{self, widget, Alignment, Length},
    IcedContext,
};

use crate::game::{GameState, GameplayConfig};

pub struct MainMenuPlugin;

impl Plugin for MainMenuPlugin {
    fn build(&self, app: &mut App) {
        app.add_event::<MainMenuEvent>()
            .add_event::<ConfigurationEvent>()
            .add_state(MainMenuState::Disabled)
            .add_system_set(
                SystemSet::on_enter(GameState::MainMenu).with_system(set_default_main_menu_state),
            )
            .add_system_set(
                SystemSet::on_update(MainMenuState::Menu)
                    .with_system(handle_main_menu_message)
                    .with_system(show_main_menu),
            )
            .add_system_set(
                SystemSet::on_update(MainMenuState::Configuration)
                    .with_system(show_configuration)
                    .with_system(handle_configuration_message),
            );
    }
}

#[derive(Hash, PartialEq, Eq, Clone, Debug)]
enum MainMenuState {
    Disabled,
    Menu,
    Configuration,
}

#[derive(Clone)]
enum MainMenuEvent {
    StartPressed,
    QuitPressed,
}

#[derive(Clone)]
enum ConfigurationEvent {
    MazeSizeChanged(i32),
    PlayPressed,
}

fn set_default_main_menu_state(mut state: ResMut<State<MainMenuState>>) {
    state.set(MainMenuState::Menu).unwrap();
}

fn show_main_menu(mut ctx: IcedContext<MainMenuEvent>) {
    ctx.display(
        crate::fill_container(
            iced::column!(
                widget::text("MAZE").size(72),
                widget::vertical_space(Length::Units(32)),
                widget::button("Start").on_press(MainMenuEvent::StartPressed),
                widget::vertical_space(Length::Units(16)),
                widget::button("Quit").on_press(MainMenuEvent::QuitPressed),
            )
            .align_items(Alignment::Center),
        )
        .center_x()
        .center_y(),
    );
}

fn show_configuration(mut ctx: IcedContext<ConfigurationEvent>, config: Res<GameplayConfig>) {
    ctx.display(
        crate::fill_container(
            iced::column!(
                widget::text("Select maze size"),
                widget::text(format!("{}", config.maze_size)),
                widget::slider(
                    5..=25,
                    config.maze_size as i32,
                    ConfigurationEvent::MazeSizeChanged,
                )
                .width(Length::Units(256)),
                widget::button("Play").on_press(ConfigurationEvent::PlayPressed),
            )
            .align_items(Alignment::Center),
        )
        .center_x()
        .center_y(),
    );
}

fn handle_main_menu_message(
    mut state: ResMut<State<MainMenuState>>,
    mut exit_events: EventWriter<AppExit>,
    mut events: EventReader<MainMenuEvent>,
) {
    for event in events.iter() {
        match event {
            MainMenuEvent::QuitPressed => exit_events.send(AppExit::default()),
            MainMenuEvent::StartPressed => state.set(MainMenuState::Configuration).unwrap(),
        }
    }
}

fn handle_configuration_message(
    mut config: ResMut<GameplayConfig>,
    mut menu_state: ResMut<State<MainMenuState>>,
    mut game_state: ResMut<State<GameState>>,
    mut events: EventReader<ConfigurationEvent>,
) {
    for event in events.iter() {
        match event {
            ConfigurationEvent::MazeSizeChanged(new_size) => config.maze_size = *new_size as usize,
            ConfigurationEvent::PlayPressed => {
                menu_state.set(MainMenuState::Disabled).unwrap();
                game_state.set(GameState::Playing).unwrap();
            }
        }
    }
}
