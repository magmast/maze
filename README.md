# Maze

Simple maze game for my doughter, because she has to play as cat :) It's written
in Rust and Bevy.

![Gameplay](gameplay.gif)

## Todo list

- [] Allow to choose a cat.

## Development

Like any other Rust project. Just edit the code and run:

```sh
cargo run
```

To see the changes.

## Releasing

Build with `--release` flag:

```sh
cargo build --release
```

And ship the binary `target/release/maze` together with `assets` folder.

## License

Project is licensed under GPLv3 license. See [LICENSE](LICENSE) file for
details.
